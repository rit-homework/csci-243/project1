CFLAGS=-Wall -Wextra -pedantic -std=c99

all: wildfire

wildfire: wildfire.c display.c
	$(CC) -o wildfire wildfire.c display.c

clean:
	rm *.o wildfire

.PHONY: clean
