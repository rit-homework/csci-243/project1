#define _BSD_SOURCE
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "display.h"

/// An enum used to indicate whether or not something succeeded.
typedef enum {
    /// Everything is ok.
    Ok,

    /// There was an error.
    Err,
} Result;

/// The states each tile on the board can be in.
typedef enum {
    /// Represents an empty state.
    EMPTY,

    /// Represents an unburnt tree.
    TREE,

    /// Represents a tree which is on fire.
    BURNING,

    /// Represents a tree which is burnt.
    BURNT,
} State;

/// Colors which terminal output could be.
typedef enum {
    /// Resets the color.
    RESET = 0,

    /// Represents a light red color.
    LIGHT_RED = 91,

    /// Represents a light green color.
    LIGHT_GREEN = 92,

    /// Represents a light yellow color.
    LIGHT_YELLOW = 93,
} Color;

/// Prints an escape string for the specified sequence.
static void print_escape_string(int sequence) { printf("\033[%dm", sequence); }

/// Prints an escape string, the string and the reset sequence to standard
/// output.
static void print_color_string(char *text, Color color) {
#ifdef COLOR_ENABLED
    print_escape_string(color);
#endif
    printf("%s", text);
#ifdef COLOR_ENABLED
    print_escape_string(RESET);
#endif
}

/// Given a state, get's the char which represents it on the board.
static void print_str_for_state(State state) {
    switch (state) {
    case TREE:
        print_color_string("Y", LIGHT_GREEN);
        break;

    case BURNING:
        print_color_string("*", LIGHT_RED);
        break;

    case BURNT:
        print_color_string("_", LIGHT_YELLOW);
        break;

    case EMPTY:
    default:
        printf("%s", " ");
    }
}

/// The help message which is output when incorrect arguments are used.
static const char *HELP = "\
usage: wildfire [-pN] size probability treeDensity proportionBurning\n\
The -pN option tells the simulation to print N cycles and stop.\n\
The probability is the probability a tree will catch fire.\n\
";

/// The size of the board.
static int size;

/// The probability that a tree will catch fire if other constraints are
/// satisfied. It is a value from 0-100.
static int probability;

/// The probability that a tree will be placed at a spot on the board. It is a
/// value from 0-100.
static int tree_density;

/// The probability that a tree will start off on fire. This is a value from
/// 0-100.
static int proportion_burning;

/// A nullable container for the maximum number of iterations to compute.
static int *max_iterations = NULL;

/// The value of the the nullable container #max_iterations.
static int max_iterations_value;

/// Returns true when #str begins with #pre.
static bool has_prefix(const char *pre, const char *str) {
    return strncmp(pre, str, strlen(pre)) == 0;
}

/// Randomly returns true n% of the time and false otherwise.
static bool decision(int n) {
    return ((float)n / 100.0) > ((float)rand() / (float)RAND_MAX);
}

/// A container for information about an argument.
typedef struct {
    /// The name of the parameter. This is used when printing the rejection.
    const char *name;

    /// The lower bound of the parameter.
    int lower_bound;

    /// The upper bound of the parameter.
    int upper_bound;

    /// The address at which the result of the parsed value will be stored.
    int *output;
} IntegerArgument;

/// The arguments (along with their constraints) this program uses.
static const IntegerArgument arg_defs[] = {
    {.name = "size", .lower_bound = 5, .upper_bound = 40, .output = &size},
    {
        .name = "probability",
        .lower_bound = 0,
        .upper_bound = 100,
        .output = &probability,
    },
    {
        .name = "density",
        .lower_bound = 0,
        .upper_bound = 100,
        .output = &tree_density,
    },
    {
        .name = "proportion",
        .lower_bound = 0,
        .upper_bound = 100,
        .output = &proportion_burning,
    },
};

/// A helper to print an out of bounds message for an IntegerArgument to
/// standard error.
static void print_bounds_message(IntegerArgument argument, int received) {
    fprintf(stderr, "The %s (%d) must be an integer in [%d...%d].\n",
            argument.name, received, argument.lower_bound,
            argument.upper_bound);
}

/// Parses the arguments defined by #arg_defs.
static Result parse(int argc, char **argv) {
    // Skip Program Name
    argc -= 1;
    argv = &argv[1];

    if (argc < 1) {
        return Err;
    }

    // Try to parse first argument.
    const char *possible_flag = argv[0];

    if (has_prefix("-p", possible_flag)) {
        // Handle Iteration Count Flag
        int prefix_size = 2;
        int base10 = 10;
        int result = (int)strtol(possible_flag + prefix_size, NULL, base10);
        if (result == 0) {
            fprintf(stderr, "The -pN option was invalid.\n");
            return Err;
        }

        if (result < 0) {
            fprintf(stderr, "The -pN option was negative.\n");
            return Err;
        }

        max_iterations_value = result;
        max_iterations = &max_iterations_value;

        // Consume Flag
        argv = &argv[1];
        argc -= 1;
        if (argc < 1) {
            return Err;
        }
    }

    // Parse Required Positional Arguments
    for (unsigned int arg_index = 0;
         arg_index < sizeof(arg_defs) / sizeof(arg_defs[0]); arg_index++) {
        if (arg_index >= (unsigned int)argc) {
            return Err;
        }

        const char *arg = argv[arg_index];
        IntegerArgument constraints = arg_defs[arg_index];

        int parsed_arg = (int)strtol(arg, NULL, 10);
        if (parsed_arg < constraints.lower_bound ||
            parsed_arg > constraints.upper_bound) {
            print_bounds_message(constraints, parsed_arg);
            return Err;
        }

        // Store Parsed, Validated Argument
        *constraints.output = parsed_arg;
    }

    return Ok;
}

/// Given the current state, the number of neighbors and the number of burning
/// neighbors, computes the next state.
static State get_next_state(State current, int neighbors,
                            int burning_neighbors) {
    if (current == BURNING && decision(probability)) {
        // Go to next state.
        return BURNT;
    }

    if (current == TREE &&
        ((float)burning_neighbors / (float)neighbors) >= 0.25f &&
        decision(probability)) {
        return BURNING;
    }

    return current;
}

/// A descriptor for a direction.
typedef struct {
    /// An offset in the y direction.
    int offset_y;

    // An offset in the x direction.
    int offset_x;
} Direction;

/// All directions where neighbors may exist.
static const Direction directions[] = {
    {.offset_y = 1, .offset_x = 0},  {.offset_y = 1, .offset_x = 1},
    {.offset_y = 0, .offset_x = 1},  {.offset_y = -1, .offset_x = 1},
    {.offset_y = -1, .offset_x = 0}, {.offset_y = -1, .offset_x = -1},
    {.offset_y = 0, .offset_x = -1}, {.offset_y = 1, .offset_x = -1},
};

/// The number of elements in the directions array.
static const int directions_count = sizeof(directions) / sizeof(directions[0]);

/// Counts the number of neighbors and number of burning neighbors on a board.
static void count_neighbors_board(State board[size][size], const int row,
                                  const int col, int *neighbors,
                                  int *burning_neighbors) {
    // Establish Pre-Conditions For Output Variables
    *neighbors = 0;
    *burning_neighbors = 0;

    for (int i = 0; i < directions_count; i++) {
        Direction direction = directions[i];
        // Check each direction.

        int check_row = row + direction.offset_y;
        if (check_row >= size) {
            continue;
        }

        int check_col = col + direction.offset_x;
        if (check_col >= size) {
            continue;
        }

        // Update Counters
        (*neighbors)++;
        State neighbor = board[check_row][check_col];
        if (neighbor == BURNING) {
            (*burning_neighbors)++;
        }
    }
}

/// Updates the board's current state to board's next state. #changes and
/// #burning are output variables.
static void next_board(State current[size][size], int *changes, int *burning) {
    // A temporary board where the next state will be created.
    State next_board[size][size];

    // Initialize Pre-Conditions
    *changes = 0;
    *burning = 0;

    for (int row = 0; row < size; row++) {
        for (int col = 0; col < size; col++) {
            // Loop over each space in board.

            // Count Neighbors
            int neighbors = 0;
            int burning_neighbors = 0;
            count_neighbors_board(current, row, col, &neighbors,
                                  &burning_neighbors);

            // Get Previous State
            State previous_state = current[row][col];

            // Compute Next State
            State next_state =
                get_next_state(previous_state, neighbors, burning_neighbors);

            // Count Change
            if (previous_state != next_state) {
                (*changes)++;
            }

            if (next_state == BURNING) {
                (*burning)++;
            }

            // Store In Temporary Board
            next_board[row][col] = next_state;
        }
    }

    // Copy From Temporary Board to Board
    for (int row = 0; row < size; row++) {
        for (int col = 0; col < size; col++) {
            current[row][col] = next_board[row][col];
        }
    }
}

/// Whether or not flags where specified to make the output cursor controlled.
static bool is_cursor_controlled() { return max_iterations == NULL; }

// Whether or not a next generation should be calculated.
static bool should_continue(int generation) {
    if (is_cursor_controlled()) {
        return true;
    }

    return generation <= *max_iterations;
}

/// Initializes the board, lighting some trees on fire.
static void initialize_board(State board[][size]) {
    for (int row = 0; row < size; row++) {
        for (int col = 0; col < size; col++) {
            // Initialize Board To Empty
            board[row][col] = EMPTY;

            if (!decision(tree_density)) {
                // Skip Some Spots
                continue;
            }

            // Place Tree
            board[row][col] = TREE;

            if (!decision(proportion_burning)) {
                // Skip Burning Some Trees
                continue;
            }

            board[row][col] = BURNING;
        }
    }
}

/// Prints ever character of the board.
static void print_board(State board[][size]) {
    for (int row = 0; row < size; row++) {
        for (int col = 0; col < size; col++) {
            State state = board[row][col];
            print_str_for_state(state);
        }

        putchar('\n');
    }
}

/// Displays a board, clearing the screen if needed.
static void display_board(State board[][size]) {
    if (is_cursor_controlled()) {
        set_cur_pos(1, 1);
    }

    print_board(board);
}

/// A helper to print the iteration footer.
static void print_footer(int generation, int changes) {
    printf(
        "cycle %d, size %d, probability %.2f, density %.2f, proportion %.2f, "
        "changes %d\n",
        generation, size, probability / 100.f, tree_density / 100.f,
        proportion_burning / 100.f, changes);
}

/// A helper to print the conclusion message.
static void print_conclusion(int total_changes) {
    printf("fires are out after %d cumulative changes.\n", total_changes);
}

/// Parses arguments and starts simulation.
int main(const int argc, char *argv[]) {
    // Parse Arguments
    if (parse(argc, argv) == Err) {
        fprintf(stderr, "%s", HELP);
        return Err;
    }

    // Seed Random Number Generator
    unsigned int seed = 10;
    srand(seed);

    int rows = size;
    int cols = size;

    // Initialize Board
    State board[rows][cols];
    initialize_board(board);

    // Clear Screen If Needed
    if (is_cursor_controlled()) {
        clear();
    }

    // Print Initial Board
    display_board(board);
    print_footer(0, 0);

    int burning = 1;
    int total_changes = 0;

    for (int generation = 1; burning > 0 && should_continue(generation);
         generation++) {
        // Generate Generations
        int changes = 0;
        next_board(board, &changes, &burning);
        total_changes += changes;

        // Display Board
        display_board(board);
        print_footer(generation, changes);

        // Sleep For A Bit
        usleep(81000);
    }

    // Print the conclusion if the end has been reached.
    if (burning == 0) {
        print_conclusion(total_changes);
    }

    return Ok;
}
